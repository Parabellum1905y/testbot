FROM openjdk:8-jre-alpine
COPY target/scala-2.12/dicoinerBot-assembly*.jar /app/application.jar

CMD ["/usr/bin/java", "-jar", "/app/application.jar"]