package me.luger.dicoiner.bot.config

import com.typesafe.config.{Config, ConfigFactory}

import scala.io.Source

/**
  * @author luger. Created on 27.09.17.
  * @version ${VERSION}
  */
case class BotConfig (botToken:String)

object ConfigLoad {
  def apply(): BotConfig = botConfig

  def keyFromEnv = sys.env.get("TELEGRAM_KEY")

  def applyConfig ():BotConfig = {
    val token = if (config.getString("bot-token").isEmpty)
      keyFromEnv.getOrElse(Source.fromFile("telegram.key").getLines().next)//TODO add checking file
    else config.getString("bot-token")
    BotConfig(token)
  }

  private[this] lazy val config:Config = ConfigFactory.load()
  private[this] lazy val botConfig:BotConfig =applyConfig()
}
