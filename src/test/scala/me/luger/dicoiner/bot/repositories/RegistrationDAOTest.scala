package me.luger.dicoiner.bot.repositories

import com.dimafeng.testcontainers.{ForAllTestContainer, GenericContainer}
import me.luger.dicoiner.bot.model._
import org.scalatest.FlatSpec
import org.slf4s.Logging
import org.testcontainers.containers.wait.Wait

import scala.concurrent.Await
import scala.concurrent.duration.Duration
/**
  * @author luger. Created on 28.09.17.
  * @version ${VERSION}
  */
class RegistrationDAOTest extends FlatSpec with Logging  with ForAllTestContainer {

  override val container = GenericContainer("mongo:latest",
    exposedPorts = Seq(27017),
    waitStrategy = Wait.forListeningPort()
  )
  lazy val freelancerDAO: FreelancerDAO = new FreelancerDAO(Option ("localhost"), Option(container.container.getMappedPort(27017)))
  lazy val registrationDAO:RegistrationDAO = new RegistrationDAO(Option ("localhost"), Option(container.container.getMappedPort(27017)))


  it should "saveRegOperation" in{
      val res = Await.result(registrationDAO.saveRegOperation(123L, RegStatus(registered = true, status = UserRegStatus.registered)), Duration.Inf)
      log.info(s"$res")
      //assert(res.isObjectId)
      //assert(res.asObjectId().getValue.toHexString.nonEmpty)
  }

  it should "testGetCurrentRegOperation" in {
      Await.result(registrationDAO.saveRegOperation(123L, RegStatus(registered = true, status = UserRegStatus.registered)), Duration.Inf)
      val res = Await.result(registrationDAO.getCurrentRegOperation(123L), Duration.Inf)
      //subscribe((x:Document) => println(x.get("tgNick")))
      log.info(s"$res")
      assert(res.isDefined)
  }

  it should "get info about all users" in {
      val res = Await.result(registrationDAO.getAll, Duration.Inf)
      //subscribe((x:Document) => println(x.get("tgNick")))
      log.info(s"$res")
      assert(res.nonEmpty)
  }

}
